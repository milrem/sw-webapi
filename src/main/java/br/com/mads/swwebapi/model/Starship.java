/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * A Starship.
 * This model is a single transport craft that has hyperdrive capability.
 * 
 * @author milrem
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class Starship extends Vehicle {
    @JsonProperty("starship_class")
    private String starshipClass;

    @JsonProperty("hyperdrive_rating")
    private String hyperdriveRating;

    @JsonProperty("MGLT")
    private String mglt;
}
