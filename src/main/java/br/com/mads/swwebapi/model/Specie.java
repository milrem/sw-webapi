/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

/**
 * A species within the Star Wars universe.
 * This model is a type of person or character within the Star Wars Universe.
 * 
 * @author milrem
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class Specie {
    private String name;
    private String classification;
    private String designation;

    @JsonProperty("average_height")
    private String averageHeight;

    @JsonProperty("average_lifespan")
    private String averageLifespan;

    @JsonProperty("eye_colors")
    private String eyeColors;

    @JsonProperty("hair_colors")
    private String hairColors;

    @JsonProperty("skin_colors")
    private String skinColors;

    @JsonProperty("homeworld")
    private String homeWorld;

    private String language;

    @JsonProperty("people")
    private List<String> peopleUrls;

    @JsonProperty("films")
    private List<String> filmsUrls;
}
