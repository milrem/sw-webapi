/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

/**
 * A planet.
 * This model is a large mass, planet or planetoid in the Star Wars Universe, at the time of 0 ABY.
 * 
 * @author milrem
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class Planet {
    private String name;
    private String diameter;
    private String gravity;
    private String population;
    private String climate;
    private String terrain;

    @JsonProperty("rotation_period")
    private String rotationPeriod;

    @JsonProperty("orbital_period")
    private String orbitalPeriod;

    @JsonProperty("surface_water")
    private String surfaceWater;

    @JsonProperty("residents")
    private List<String> residentsUrls;

    @JsonProperty("films")
    private List<String> filmsUrls;
}
