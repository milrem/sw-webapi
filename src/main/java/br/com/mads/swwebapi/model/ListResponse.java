/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.Data;

/**
 * This model is just for help in reading the SWAPI list responses.
 * 
 * @author milrem
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class ListResponse<T> {
    private int count;
    private String next;
    private String previous;
    private List<T> results;
}
