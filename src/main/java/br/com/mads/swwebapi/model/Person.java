/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

/**
 * A person within the Star Wars universe.
 * This model is an individual person or character within the Star Wars universe.
 * 
 * @author milrem
 */
@ApiModel(
    value = "Personagens", 
    description = "Informações detalhadas do personagem"
)
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class Person {
    @ApiModelProperty(value = "Nome do personagem")
    private String name;
    @ApiModelProperty(value = "Ano de nascimento")
    @JsonProperty("birt_year")
    private String birthYear;
    @ApiModelProperty(value = "Cor dos olhos")
    @JsonProperty("eye_color")
    private String eyeColor;
    @ApiModelProperty(value = "Sexo")
    private String gender;
    @ApiModelProperty(value = "Cor dos cabelos")
    @JsonProperty("hair_color")
    private String hairColor;
    @ApiModelProperty(value = "Altura em centímetros")
    private String height;
    @ApiModelProperty(value = "Peso em kilogramas")
    private String mass;
    @ApiModelProperty(value = "Cor da pele")
    @JsonProperty("skin_color")
    private String skinColor;
    @ApiModelProperty(value = "Url para acesso as informações da terra natal ou local de moradia")
    @JsonProperty("homeworld")
    private String homeWorldUrl;
    @ApiModelProperty(value = "Url para acesso as informações dos filmes que esse personagem aparece")
    @JsonProperty("films")
    private List<String> filmsUrls;
    @ApiModelProperty(value = "Url para acesso as informações da espécie do personagem")
    @JsonProperty("species")
    private List<String> speciesUrls;
    @ApiModelProperty(value = "Url para acesso as informações das naves estelares que este personagem pilota")
    @JsonProperty("starships")
    private List<String> starshipsUrls;
    @ApiModelProperty(value = "Url para acesso as informações dos veículos este personagem utiliza")
    @JsonProperty("vehicles")
    private List<String> vehiclesUrls;
    @ApiModelProperty(value = "Url para acesso as informações do personagem diretamente na SWAPI")
    private String url;
}
