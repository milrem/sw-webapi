/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

/**
 * A vehicle.
 * This model is a single transport craft that does not have hyperdrive capability.
 * 
 * @author milrem
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class Vehicle {
    private String name;
    private String model;

    @JsonProperty("vehicle_class")
    private String vehicleClass;

    private String manufacturer;

    @JsonProperty("cost_in_credits")
    private String costInCredits;

    private String length;
    private String crew;
    private String passengers;

    @JsonProperty("max_atmosphering_speed")
    private String maxAtmospheringSpeed;

    @JsonProperty("cargo_capacity")
    private String cargoCapacity;

    private String consumables;

    @JsonProperty("pilots")
    private List<String> pilotsUrls;

    @JsonProperty("films")
    private List<String> filmsUrls;
}
