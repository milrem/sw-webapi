/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 * A Star Wars film.
 * This model is a single film.
 * 
 * @author milrem
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public @Data class Film {
    private String title;

    @JsonProperty("episode_id")
    private int episodeId;

    @JsonProperty("opening_crawl")
    private String openingCrawl;

    private String director;
    private String producer;
    
    @JsonProperty("release_date")
    private Date releaseDate;

    @JsonProperty("species")
    private List<String> speciesUrls;

    @JsonProperty("starships")
    private List<String> starshipsUrls;

    @JsonProperty("vehicles")
    private List<String> vehiclesUrls;

    @JsonProperty("planets")
    private List<String> planetsUrls;

    @JsonProperty("characters")
    private List<String> charactersUrls;
}
