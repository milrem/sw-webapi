/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.security.jwt;

import org.springframework.security.core.AuthenticationException;

/**
 * Exception thrown when the JWT is malformed or invalid.
 * @author milrem
 */
public class JwtTokenMalformedException extends AuthenticationException {
    public JwtTokenMalformedException(String msg) {
		super(msg);
	}
}
