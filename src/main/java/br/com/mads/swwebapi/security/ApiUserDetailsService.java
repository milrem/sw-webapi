/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.security;

import br.com.mads.swwebapi.data.UserRepository;
import br.com.mads.swwebapi.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author milrem
 */
@Service
public class ApiUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository repository;
    
    ApiUserDetailsService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = repository.findByCpf(username);
        if (user == null)
            throw new UsernameNotFoundException(username);
        return new ApiUserPrincipal(user);
    }
}
