/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.security;

import br.com.mads.swwebapi.model.Role;
import br.com.mads.swwebapi.model.User;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author milrem
 */
public class ApiUserPrincipal implements UserDetails {
    private final String username;
    private final String password;
    private final String[] roles;
    
    public ApiUserPrincipal(String username, String password, String[] roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }
    
    public ApiUserPrincipal(User user) {
        this.username = user.getCpf();
        this.password = user.getPwd();
        this.roles = user.getRoles().stream().map(role -> "ROLE_".concat(role.getName())).collect(Collectors.joining(",")).split(",");
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.stream(roles).map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    
}
