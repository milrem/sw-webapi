/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.security.jwt;

import org.springframework.security.core.AuthenticationException;

/**
 * Exception thrown when the JWT is nor found in request header.
 * @author milrem
 */
public class JwtTokenMissingException extends AuthenticationException {
    public JwtTokenMissingException(String msg) {
		super(msg);
	}
}
