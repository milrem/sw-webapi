/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.security.jwt;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author milrem
 */
@ApiModel(
    value = "Credenciais do usuário", 
    description = "Credenciais do usuário, utilizadas para efetuar o login e receber o token de acesso as demais funcionalidade da API"
)
public @Data class JwtRequest {
    @ApiModelProperty(value = "Identificação do usuario", notes = "Deve ser informado o CPF")
    private String username;
    @ApiModelProperty(value = "Senha do usuario")
    private String password;
}
