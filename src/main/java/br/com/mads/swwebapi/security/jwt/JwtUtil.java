/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.security.jwt;

import br.com.mads.swwebapi.security.ApiUserPrincipal;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.Date;
import java.util.stream.Collectors;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author milrem
 */
public class JwtUtil {
    private static final String JWT_SECRET = "@N3v3s@";
    private static final long TOKEN_EXPIRATION = 30*60*1000;
    
    public static UserDetails parseToken(String token) {
        try {
            validateToken(token);
            Claims body = Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(token).getBody();
            return new ApiUserPrincipal(body.getSubject(), null, body.get("roles", String.class).split(","));
        } catch (Exception e) {
            throw new JwtTokenMalformedException("Invalid JWT token (2)");
        }
    }

    public static String generateToken(ApiUserPrincipal user) {
        long nowMillis = System.currentTimeMillis();
        long expMillis = nowMillis + TOKEN_EXPIRATION;
        Date expiration = new Date(expMillis);
        
        Claims claims = Jwts.claims().setSubject(user.getUsername());
        claims.put("roles", user.getAuthorities().stream().map(a -> a.getAuthority()).collect(Collectors.joining(",")));
        
        return Jwts.builder().setClaims(claims)
                .setIssuedAt(new Date(nowMillis))
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
    }

    public static void validateToken(final String token) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(token);
        } catch (SignatureException ex) {
            throw new JwtTokenMalformedException("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            throw new JwtTokenMalformedException("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            throw new JwtTokenMalformedException("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            throw new JwtTokenMalformedException("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            throw new JwtTokenMissingException("JWT claims string is empty.");
        }
    }
}
