/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.security.jwt;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author milrem
 */
@ApiModel(
    value = "Token de acesso as funcionalidades da API", 
    description = "Joken de autorização utilizado no header das requisições para acesso as demais funcionalidade da API"
)
public @Data class JwtResponse {
    @ApiModelProperty(value = "Bearer token do usuario")
    private final @NonNull String token;
}
