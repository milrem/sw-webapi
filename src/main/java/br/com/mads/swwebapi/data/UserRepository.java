/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.data;

import br.com.mads.swwebapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author milrem
 */
public interface UserRepository extends JpaRepository<User, String>{
    User findByCpf(String cpf);
}
