/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.business;

import br.com.mads.swwebapi.model.Film;
import br.com.mads.swwebapi.model.ListResponse;
import br.com.mads.swwebapi.model.Person;
import br.com.mads.swwebapi.model.Specie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author milrem
 */
@Service
public class SwApiClient {
    private final static String SWAPI_ENDPOINT = "https://swapi.co/api/";
    
    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(10*1000);
        factory.setReadTimeout(10*1000);
        return factory;
    }
    
    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "SW-WebApi/0.1");
        headers.add("accept", "application/json");
        return headers;
    }
    
    private String readPeople(String url, List<Person> outList) {
        ResponseEntity<ListResponse<Person>> response = new RestTemplate(getClientHttpRequestFactory()).exchange(url, HttpMethod.GET, new HttpEntity<String>(getHeaders()), new ParameterizedTypeReference<ListResponse<Person>>() {});
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            ListResponse<Person> respBody = response.getBody();
            outList.addAll(respBody.getResults());
            return respBody.getNext();
        }
        return null;
    }
    
    private String readSpecies(String url, List<Specie> outList) {
        ResponseEntity<ListResponse<Specie>> response = new RestTemplate(getClientHttpRequestFactory()).exchange(url, HttpMethod.GET, new HttpEntity<String>(getHeaders()), new ParameterizedTypeReference<ListResponse<Specie>>() {});
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            ListResponse<Specie> respBody = response.getBody();
            outList.addAll(respBody.getResults());
            return respBody.getNext();
        }
        return null;
    }
    
    public List<Person> getAllPeople() {
        List<Person> lista = new ArrayList<>();
        String url = SWAPI_ENDPOINT.concat("people/");
        do {
            url = readPeople(url, lista);
        } while (url != null);
        return lista;
    }
    
    public Person getPersonById(String id) {
        return getPersonByUrl(SWAPI_ENDPOINT.concat("people/").concat(id));
    }
    
    public Person getPersonByUrl(String url) {
        ResponseEntity<Person> response = new RestTemplate(getClientHttpRequestFactory()).exchange(url, HttpMethod.GET, new HttpEntity<String>(getHeaders()), Person.class);
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            return response.getBody();
        }
        return null;
    }
    
    public Film getFilmByUrl(String url) {
        ResponseEntity<Film> response = new RestTemplate(getClientHttpRequestFactory()).exchange(url, HttpMethod.GET, new HttpEntity<String>(getHeaders()), Film.class);
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            return response.getBody();
        }
        return null;
    }
    
    public List<Specie> getSpeciesByName(String name) {
        List<Specie> lista = new ArrayList<>();
        String url = SWAPI_ENDPOINT.concat("species/").concat("?search=").concat(name);
        do {
            url = readSpecies(url, lista);
        } while (url != null);
        return lista;
    }
    
}
