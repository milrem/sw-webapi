/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author milrem
 */
@ApiModel(
    value = "Personagens e filmes", 
    description = "Nome e ano de nascimento do personagem e lista dos nomes dos filmes que este personagem aparece"
)
public @Data class PersonFilms {
    @ApiModelProperty(value = "Nome", notes = "Nome do personagem", example = "C-3PO")
    @NonNull
    private String nome;
    @ApiModelProperty(value = "Ano de nascimento do personagem", notes = "Caso não seja conhecido ou não seja relevante será apresentado 'n/a'", example = "112BBY")
    @JsonProperty("ano_nascimento")
    private String anoNascimento;
    @ApiModelProperty(value = "Nomes dos filmes", notes = "Lista dos nomes dos filmes que este personagem aparece", example = "[\"https://www.swapi.co/api/films/2/\", \"https://www.swapi.co/api/films/5/\"]")
    private List<String> films;
}
