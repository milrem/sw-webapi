/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.controller;

import br.com.mads.swwebapi.model.Person;
import br.com.mads.swwebapi.model.Specie;
import br.com.mads.swwebapi.business.SwApiClient;
import br.com.mads.swwebapi.controller.dto.PersonFilms;
import br.com.mads.swwebapi.security.RoleUsuario;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author milrem
 */
@Api(value = "Operações relacionadas aos personages da saga Star Wars")
@ApiResponses({
    @ApiResponse(code = 200, message = "Operação realizada com sucesso"),
    @ApiResponse(code = 401, message = "Usuário sem permissão de acesso, verifique as credenciais do usuário"),
    @ApiResponse(code = 403, message = "Usuário sem permissão de acesso, verifique o perfil usuário")
})
@RestController
@RequestMapping(path = "api/personagens")
@RoleUsuario
public class PersonagensController {
    
    @Autowired
    private SwApiClient client;
    
    @ApiOperation("Listar todos os personagens ordenados pela quantidade de filmes em que aparecem")
    @GetMapping()
    public ResponseEntity<List<Person>> getOrderedByFilms() {
        List<Person> lista = client.getAllPeople();
        lista.sort((p1, p2) -> {
            int r = Integer.compare(p1.getFilmsUrls().size(), p2.getFilmsUrls().size());
            if (r == 0) {
                r = p1.getName().compareToIgnoreCase(p2.getName());
            } else {
                r = r * -1;
            }
            return r;
        });
        return ResponseEntity.ok(lista);
    }
    
    @ApiOperation("Obter o nome e ano de nascimento de um personagem pelo ID juntamente com o nome de todos os films em que ele aparece")
    @GetMapping(path = "{id}")
    public ResponseEntity<PersonFilms> getById(@PathVariable("id") String id) {
        Person person = client.getPersonById(id);
        if (person == null)
            return ResponseEntity.notFound().build();
        PersonFilms resp = new PersonFilms(person.getName());
        resp.setAnoNascimento(person.getBirthYear());
        resp.setFilms(person.getFilmsUrls().stream()
                .map(client::getFilmByUrl)
                .filter(f -> f != null)
                .sorted((f1, f2) -> Integer.compare(f1.getEpisodeId(), f2.getEpisodeId()))
                .map(film -> film.getTitle())
                .collect(Collectors.toList())
        );
        return ResponseEntity.ok(resp);
    }
    
    @ApiOperation("Retorna o peso médio de todos os personagens humanos")
    @GetMapping(path = "humans/mass")
    public ResponseEntity<Double> getHumansAverageMass() {
        List<Specie> lista = client.getSpeciesByName("Human");
        double averageMass = 0;
        for (Specie specie : lista) {
            averageMass = specie.getPeopleUrls().stream()
                    .map(client::getPersonByUrl)
                    .mapToDouble(person -> Arrays.asList("unknown", "n/a").contains(person.getMass()) ? 0 : Float.parseFloat(person.getMass()))
                    .filter(mass -> mass > 0)
                    .average().orElse(0);
        }
        return ResponseEntity.ok(BigDecimal.valueOf(averageMass).setScale(2, RoundingMode.HALF_UP).doubleValue());
    }
}
