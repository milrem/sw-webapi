/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.controller.dto;

import br.com.mads.swwebapi.model.Role;
import br.com.mads.swwebapi.model.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Data;
import lombok.NonNull;

/**
 *
 * @author milrem
 */
@ApiModel(
    value = "Usuário e perfis de acesso", 
    description = "Identificação do usuário e lista de perfis de acesso ativos"
)
public @Data class UsuarioPerfis {
    @ApiModelProperty(value = "Identificação do usuario", notes = "Informado o CPF")
    @NonNull
    private final String cpf;
    @ApiModelProperty(value = "Perfis de acesso", notes = "Lista de perfis de acesso ativos para o usuario")
    private final List<String> perfis;
    
    public UsuarioPerfis(User user) {
        cpf = user.getCpf();
        perfis = user.getRoles().stream().map(Role::getName).collect(Collectors.toList());
    }
}
