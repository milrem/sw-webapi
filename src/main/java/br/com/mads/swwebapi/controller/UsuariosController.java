/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mads.swwebapi.controller;

import br.com.mads.swwebapi.controller.dto.UsuarioPerfis;
import br.com.mads.swwebapi.data.UserRepository;
import br.com.mads.swwebapi.security.RoleAdmin;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author milrem
 */
@Api(value = "Operações relacionadas a gestão de usuários")
@ApiResponses({
    @ApiResponse(code = 200, message = "Operação realizada com sucesso"),
    @ApiResponse(code = 401, message = "Usuário sem permissão de acesso, verifique as credenciais do usuário"),
    @ApiResponse(code = 403, message = "Usuário sem permissão de acesso, verifique o perfil usuário")
})
@RestController
@RequestMapping(path = "api/usuarios")
@RoleAdmin
public class UsuariosController {
    
    @Autowired
    private UserRepository repository;
    
    @ApiOperation("Lista os usuários registrados na API juntamente com seus perfis de acesso")
    @GetMapping()
    public ResponseEntity<List<UsuarioPerfis>> getUsuarios() {
        List<UsuarioPerfis> lista = repository.findAll().stream().map(UsuarioPerfis::new).collect(Collectors.toList());
        return ResponseEntity.ok(lista);
    }
}
