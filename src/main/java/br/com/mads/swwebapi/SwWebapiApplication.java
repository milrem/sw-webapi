package br.com.mads.swwebapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwWebapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwWebapiApplication.class, args);
	}

}
