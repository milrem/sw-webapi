/**
 * Author:  milrem
 * Created: 02/02/2020
 */
INSERT INTO role (name) VALUES ('USUARIO');
INSERT INTO role (name) VALUES ('ADMIN');

INSERT INTO user (cpf, pwd) VALUES ('11111111111', '$2a$10$fnikAI4uHoxw0M2vSsjjg..SR3KGuN.MErPZgE9yNqFGVgTIatAwC');
INSERT INTO user (cpf, pwd) VALUES ('22222222222', '$2a$10$fnikAI4uHoxw0M2vSsjjg..SR3KGuN.MErPZgE9yNqFGVgTIatAwC');

INSERT INTO user_role (user_id, role_id) VALUES ('11111111111', 'USUARIO');
INSERT INTO user_role (user_id, role_id) VALUES ('22222222222', 'USUARIO');
INSERT INTO user_role (user_id, role_id) VALUES ('22222222222', 'ADMIN');